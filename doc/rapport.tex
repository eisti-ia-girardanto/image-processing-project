\documentclass[a4paper]{article}

\usepackage{luatextra}
\usepackage{hyperref}
\usepackage[french]{babel}

\title{Projet YOLOSign \\
Image Processing}
\author{Antoine \textsc{Girard} \\
\texttt{antoine.girard@eisti.eu} \\ \\
EISTI - Intelligence Artificielle}

\date{7 novembre 2018}

\begin{document}
    \maketitle
    \tableofcontents

    \section{Introduction}

    En 2018, nous sommes de plus en plus nombreux à utiliser des solutions de navigation et d'aide à la navigation sur nos \emph{téléphones intelligents} plus communément appelés par le terme anglais \emph{smartphone} y compris sur des trajets quotidiens. Les concurrents sont multiples mais un acteur se démarque dans ce domaine par son nombre d'utilisateurs et la fiabilité de ses données \emph{Waze}.

    En étudiant un peu son fonctionnement, on découvre que la majorité des données utilisées par l'applications sont mise à jour par les utilisateurs eux-mêmes, que ce soit par le biais de l'application pour signaler des incidents ou des erreurs de carte, ou par le biais d'un site internet pour éditer directement la carte.

    Le système est plutôt fiable et robuste notamment grâce au nombre important d'utilisateurs. En avril 2018 nous comptions plus de 100 millions d'utilisateurs actifs de l'application\cite{noauthor_waze_2018} dont presque 10 millions en France\cite{press:waze_hassani}. Malgré cette forte communauté certains types de données peines à se mettre à jour comme la limitation de vitesse ou les zones de travaux et nécessite des actions de la part des utilisateurs. 

    \section{Problématique}

    Étant utilisateur de ce type de solutions (y compris de la partie collaborative), je me suis alors posé la question de savoir s'il était possible de rendre encore plus rapide la mise à jour de certaines données et de limiter l'action des utilisateurs sur la mise à jour de celle-ci.

    Ce module étant un module d'\emph{Image Processing}, je me suis alors immédiatement mis à penser aux systèmes de lecture de panneaux qui équipent déjà certaines voitures et je me suis demandé comment ce type de système pouvait être utile pour mettre à jour des données cartographique en temps réel.

    La problématique que j'ai donc choisi de traiter est la suivante : Est-il possible d'intégrer au sein des applications de navigation et d'aide à la navigation un système de reconnaissance de panneaux ou d'incidents afin de mettre à jour automatiquement les informations cartographiques ? \\

    Les enjeux de cette problématique sont multiples :
    \begin{enumerate}
        \item La caméra d'un téléphone portable positionnée sur le pare-brise d'un véhicule permet-elle d'obtenir des images d'une qualité suffisante pour détecter les diverses informations routières ?
        \item La puissance de l'appareil est-elle suffisante pour traiter les images acquises ?
        \item Ce type de système ne va t-il pas ralentir les autres fonctionnalités de l'application et du téléphone ?
        \item Est-il possible de conserver un poids raisonnable pour ce type d'application en embarquant un système de reconnaissance de panneaux et autres événements routiers ?
    \end{enumerate}

    Le temps alloué à cette étude étant limité, j'ai pris la décision de me concentrer sur un procéder technique et d'étudier la possibilité de son implémentation.

    \section{Objectifs technique à mettre en œuvre}

    Mon cursus d'étude pour cette année universitaire étant centrée sur l'intelligence artificielle j'ai donc décidé d'étudier une méthode de détection basée sur le \emph{Deep Learning}\cite{wiki:deep_learning} ou apprentissage profond.

    L'objectif technique que je me suis fixé sur ce projet est donc de savoir s'il est possible d'intégrer un réseau de neurones au sein d'une application mobile afin de procéder à la détection et à la lecture des panneaux. Le reste de mon étude est donc centré sur ce procédé technique et sur toutes les composantes auxiliaires du projet. \\

    Le système est donc initialement imaginé de la manière suivante. L'utilisateur positionne son téléphone avec un support sur le pare-brise de son véhicule afin de capter des images de la route. Une fois ce dispositif mis en place, l'application mobile ouverte et l'approbation de l'utilisateur d'utiliser ce système, celui-ci se met en route. Il commence à capturer des images de la route à interval régulier lorsque le véhicule est en mouvement. Dès qu'une image est capturée elle est alors traité par le réseau de neurones afin de déterminer la présence de signalisation routière et de classifier chacune d'entre elle. Les informations récoltés sont associés à des métadonnées notamment la probabilité que l'information soit correcte, la date, la position du véhicule et le sens de circulation et transmise vers les serveurs de l'application cartographique qui utilisera ce système. Une fois les informations récoltées, il est possible de les combiner avec d'autres types d'informations (comme des informations provenant d'autres utilisateurs) afin de les valider et de mettre à jour la cartographie.

    \section{Étude de l'existant}

    \subsection{Applications similaires}

    Le système de lecture de panneaux n'est pas un système nouveau. Il équipe de nombreux modèles de voitures depuis quelques années\cite{wiki:traffic_sign}. Néanmoins ce type de technologie peine à débarquer sur des appareils mobiles. Deux applications mobiles, \emph{MyDriveAssist}\cite{noauthor_application_nodate} et \emph{Road Sign Recognition}\cite{droid_road_2014} ont été mises en ligne mais ont été retirées des marchés d'applications depuis. La question qui reste en suspend dans ces deux cas est de savoir pourquoi de telles applications ont été arrêtées. La question de la fiabilité se pose. En effet, comment ce type d'application qui avait pour but d'informer l'utilisateur des signalisations qu'ils avaient rencontré pouvait garantir une fiabilité de l'information en tout temps avec uniquement la caméra du téléphone.

    \subsection{Détection et reconnaissance d'objets basée sur le \emph{Deep Learning}}

    \subsubsection{La détection d'objets}

    Les technologies de détection d'objets se répandent de plus en plus. En effet, nous sommes dans une société où nous collectons de plus en plus de données. Il nous est facile de traiter des données issues de capteurs qui nous fournissent des données binaires. Nous souhaitons automatiser de plus en plus de tâches et cela passe entre autre par le traitement de données plus complexes telles que des textes, des images ou des bandes sons. 
    Avec cette envie croissante de créer des voitures autonomes, d'analyser les images du nombre de plus en plus important de caméras de surveillance, ou encore de contrôler les accès de certaines zones de manière transparente pour les utilisateurs, il a fallu développer des algorithmes puissants permettant la détection d'objets ou de personnes sur des images.
    Il existe aujourd'hui deux approches prédominantes pour faire cela :
    \begin{itemize}
        \item L'apprentissage automatique ou \emph{Machine Learning}
        \item L'apprentissage profond ou \emph{Deep Learning}
    \end{itemize}

    Comme indiqué précédemment j'ai décidé de concentrer cette étude sur l'apprentissage profond. La raison principale de ce choix est le fait qu'il s'agisse d'une technologie plutôt récente et dont les avancées se font de plus en plus rapide ces dernières années. Ceci est notamment dû à l'augmentation des capacités de calcul.

    \subsubsection{Le \emph{Deep Learning} ou apprentissage profond}

    L'apprentissage profond est un ensemble de méthodes d'apprentissages automatiques. Il a pour vocation de modéliser des données avec un haut niveau d'abstraction. Un élément de base de cette discipline est le réseau de neurones\cite{wiki:ann}. Il s'agit de tenter d'imiter le cerveaux humain dans une certaine tâche d'apprentissage.

    \paragraph{Le réseau de neurones} Il est composé de plusieurs neurones formels connectés les uns aux autres et dont chacun d'entre eux doit effectuer un petit calcul. Ceux-ci sont disposés en couches successives dont la première couche est une couche d'entrée des données et la dernière couche est une couche de sortie permettant récupérer une prédiction. Les couches cachés permettent d'évaluer des caractéristiques des données. Chaque neurone à un poids qui est fixé par apprentissage sur des jeux de données pour lesquels nous connaissons les résultats.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[scale=0.3]{img/neural_network.png}
            \caption{Vue simplifiée d'un réseau artificiel de neurones\cite{wiki:neural_network_figure}}
        \end{center}
    \end{figure}

    Si on veut reconnaître des photos de chat on va donc créer un réseau de neurones prenant en entrée chaque pixel d'une image et après plusieurs couches successives nous aurons une couche de sortie composée d'un seul neurone qui nous indiquera si oui ou non il s'agit d'une image de chat. Pour que le réseau puisse déterminer ce résultat il faut l'avoir entraîné à reconnaître des images de chat en lui donnant pleins d'images différentes associés à un libellé indiquant le résultat attendu. À chaque fois que nous soumettons une image au réseau on calcul un coût qui nous permet d'ajuster ensuite les poids des différents neurones. De manière plus simple, dès que l'ordinateur se trompe dans la prédiction on lui dit qu'il a fait une erreur et il tente de se corriger pour la prochaine fois.

    \paragraph{Les différents types de réseaux} Il existe plusieurs types de réseaux de neurones permettant de répondre à différents types de problèmes. Ceux-ci se différencient par leur structure. Parmi ces types nous en avons notamment deux que sont :
    
    \begin{itemize}
        \item Le réseau de neurones convolutifs ou \emph{Convolutional Neural Network} (CNN ou \emph{ConvNet}) : il permet notamment de traiter les images par le principe de la convolution.
        \item Le réseau de neurones récurrents ou \emph{Recurrent Neural Network} (RNN) : il permet de traiter des données de taille variable comme des enregistrements audio ou des textes par un principe de récurrence et de mémoire long terme.
    \end{itemize} 

    Les techniques de détection d'images basées sur du \emph{Deep Learning} s'appuient sur des réseaux de neurones convolutifs. Nous allons donc aborder ce sujet qui est utile dans la comprehension des techniques de détections d'objet dans une image.

    \paragraph{Le réseau de neurones convolutifs} Lorsque nous utilisons un réseau de neurones de base, tous les neurones d'une couche sont connectés à tous les neurones de la couche précédente mais aussi de la couche suivante. Cela pose un problème de taille dans le cas du traitement des images. En effet, le nombre de calcul à effectuer peut vite devenir conséquent. Pour une image RGB carrée de 400 pixels de côté, nous avons déjà $400*400*3 = 480 000$ paramètres d'entrées. Il s'agit donc de réduire le nombre de calcul qui devront être effectués par le réseau de neurones pour limiter le temps de calcul.

    De plus, à l'instar du principe même de neurone artificiel, le réseau de neurones convolutifs est inspiré du cortex visuel des animaux en ce qui concerne la connexion entre les différents neurones. En effet, les neurones dans cette partie du cerveau sont arrangés de sorte à couvrir des régions qui se chevauchent lors du pavage du champs visuel. Cela permet de traiter de petites quantités d'information par région de pixels à proximité les uns des autres. De plus, il parait intuitif lors de l'observation d'une image de faire des observations locales pour déterminer des caractéristiques locales et par la suite de déterminer des caractéristiques globales.

    Il s'agit donc de \emph{découper l'image en plusieurs régions} appelés champs récepteurs dans lesquels on va essayer de détecter des caractéristiques. Chaque champs récepteur est traité simultanément par une première couche de convolution. Il s'agit en fait d'appliquer un filtre sur chaque champ récepteur. Ce filtre permet la détection de caractéristiques. Cette couche est suivi d'une couche de \emph{pooling} qui permet de compresser l'information en réduisant la taille de l'image résultante, ce qui a pour conséquence de réduire le nombre de paramètres pour la couche suivante – et donc le temps de calcul — ainsi que de mieux contrôler le sur-apprentissage\cite{wiki:overfitting} qui est un problème récurrent des réseaux de neurones. Les couches de convolutions et de pooling se succèdent les une après les autres. Le nombre de couches dépend de la donnée que nous souhaitons traiter, tout comme la taille des filtres de convolutions.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[scale=0.4]{img/cnn.png}
            \caption{Exemple d'architecture de réseau de neurones convolutifs\cite{wiki:cnn_figure}}
        \end{center}
    \end{figure}

    La dernière couche de ce type de réseau est une couche entièrement connectée ou \emph{fully connected} permettant de regrouper les différentes caractéristiques mise en avant par les couches de convolutions pour déterminer un résultat global sur l'image. Pour finir une couche de sortie permet de récupérer un résultat. Cette couche peut contenir un seul neurone (déterminer si oui ou non il s'agit d'une image de chat par exemple) ou plusieurs neurones (indiquer quel chiffre manuscrit est inscrit sur l'image avec des probabilités).

    \subsubsection{Algorithmes de détection d'objets basés sur le \emph{Deep Learning}}

    Il existe notamment trois approches différentes :

    \begin{itemize}
        \item \emph{Region Proposals} littéralement « proposition de régions ». L'objectif est en fait d'extraire des régions dans une image (environ 2000) et pour chaque région de détecter des caractéristiques via un CNN afin de classifier la région. Ce procéder existe en 3 versions : le \emph{R-CNN}\cite{Girshick2014RichFH}, le \emph{Fast R-CNN}\cite{DBLP:journals/corr/Girshick15} et le \emph{Faster R-CNN}\cite{DBLP:journals/corr/RenHG015}.
        \item \emph{Single Shot MultiBox Detector} (SSD) \cite{DBLP:journals/corr/LiuAESR15}. Il s'agit d'une première approche de détection d'objets basée sur un unique réseau de neurones.
        \item \emph{You Only Look Once} \cite{Redmon2016YouOL}\cite{DBLP:journals/corr/RedmonF16}\cite{DBLP:journals/corr/abs-1804-02767} L'objectif est de séparer l'image en plusieurs parties de même taille et de faire une reconnaissance sur cette partie de l'image.
    \end{itemize}

    Le point commun de ces trois procédés est qu'ils utilisent des réseaux de neurones convolutifs que nous avons vu précédemment.

    \section{Étude technique}

    \subsection{Comparatif de performances}

    Nous avons vu précédemment qu'il existe différentes méthodes pour détecter des objets dans des images grâce au \emph{Deep Learning}. Ces techniques sont toutes trois différentes et plus ou moins récentes. Compte-tenu des contraintes importantes imposées, notamment en terme de mobilité, il est préférable de choisir parmi ces méthodes celle la plus rapide qui pourrait être en mesure d'être embarquée dans une application mobile. Chaque papier présente ses performances comparés à celle des autres sur des jeux de données communs comme le jeu de données COCO\footnote{\url{http://cocodataset.org/}} ou le \textsc{Pascal} VOC\footnote{\url{http://host.robots.ox.ac.uk/pascal/VOC/}}.

    Si nous prenons les résultats du papier présentant la version 3 de \emph{YOLO} (dernier papier en date concernant les trois approches listées précédemment) nous nous apercevons qu'il ne s'agit pas de la méthode optimum dans tous les cas mais que dans le cas moyen c'est une méthode de détection qui obtient des résultats plus que correct et dans un temps rapide vis à vis des autres méthode.

    
    \subsection{Algorithme \emph{You Only Look Once}}

    L'algorithme \emph{You Only Look Once} abrégé \emph{YOLO} a été créé en 2016. Il a connu jusqu'ici deux évolutions avec la version 2 en 2017 et la version 3 en 2018. Ce procédé est considéré par beaucoup comme le moyen le plus rapide et assez efficace pour la détection d'objet dans une image ou dans un flux vidéo. Il a été entraîné avec les deux jeux de donnés les plus connus dans le domaine cités précédemment.

    \subsubsection{Le principe}

    Le principe technique de \emph{YOLO} est comme pour les autres procédé un enchaînement de couches bien spécifiques dans un réseau de neurone. En effet, comme pour beaucoup de résultats mis en avant dans le domaine du \emph{Deep Learning} il s'agit de « \emph{test \& learn} »\footnote{\url{https://en.wikipedia.org/wiki/Test_and_learn}} jusqu'à obtenir un résultat satisfaisant pour le problème à traiter.

    L'assemblage des couches ne se fait néanmoins pas complètement au hasard et une idée de base est émise pour chaque algorithme. Dans le cas de \emph{You Only Look Once} l'idée était comme son nom l'indique est de ne parcourir qu'une seule fois l'image passée en entrée du réseau de neurones et à partir de ce passage unique de détecter des objects et leur emplacement.

    Ce procédé dans sa version 1 fonctionne ainsi :

    \begin{enumerate}
        \item Redimensionnement de l'image au format $448\times448$
        \item Exécution d'un réseau de neurones convolutifs
        \item Utilisation d'une technique de suppression des données non pertinente
    \end{enumerate}

    L'unique réseau de neurones convolutifs s'occupe de détecter les zones dans lesquelles un objet est présent et d'en déterminer la classe. Pour cela, il divise l'image carré en une grille ortholinéaire de dimension $S \times S$. Pour chaque élément de cette grille, il s'agira alors de déterminer $B$ cadres de sélections appelés \emph{bounding boxes} associés à un score. Il s'agira également pour chaque case de cette grille de déterminer une classe d'objet probablement présent (comme dans l'exemple suivant : un vélo, un chien, une voiture,…). Une fois ceci fait, il est possible de combiner les cadres de sélections et de les associés avec la classe de l'élément détecté. Le résultat des prédictions sera un tenseur de taille $S \times S \times (B * 5 + C)$ avec $C$ le nombre de classes d'éléments (pour chaque cadre de sélection nous avons la probabilité associée qu'il s'agisse d'un objet ou d'un autre). $B$ est multiplié par $5$ car nous disposons de 5 informations pour chaque cadre de sélection : les coordonnées $x$ et $y$ ainsi que la dimension du cadre $w$ et $h$ et enfin la probabilité de présence d'un objet à détecter. Dans le cas du jeu de données \textsc{Pascal VOC} il y a 20 classes labellisées (voiture, chat, personnes,…). Si on place la taille de la grille à $7 \times 7$ et le nombre de cadres de sélections à 2 par cellule nous avons donc un tenseur de taille $7 \times 7 \times 30$.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[scale=0.3]{img/model_yolo.png}
            \caption{Illustration du modèle proposée dans le papier de la version 1}
        \end{center}
    \end{figure}

    Une des grandes forces de \emph{YOLO} est de faire les opérations de détection simultanément sur tous les éléments de la grille formée, avec un seul \emph{ConvNet}. Celui-ci est composé dans la version 1 de 24 couches convolutives suivies de 2 couches entièrement connectées.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[scale=0.25]{img/cnn_yolo.png}
            \caption{Illustration du réseau de neurones convolutifs proposée dans le papier de la version 1}
        \end{center}
    \end{figure}

    Les couches convolutives sont régulièrement intercalées par des couches de \emph{pooling} permettant de compresser les données. \emph{YOLO} utilise des couches de type \emph{maxpooling} qui ne gardera que le résultat le plus élevé de la dimension sélectionnée. L'expérience a montré que le \emph{maxpooling} est l'une des meilleure technique de \emph{pooling} dans la majorité des cas.

    \subsubsection{Les améliorations}

    Dans les versions 2 et 3 de \emph{YOLO} de nombreuses améliorations ont été apportés mais le principe est resté le même. Les objectifs des concepteurs étaient de rendre cette méthode de détection d'objets plus rapide et plus fiable. Les hyper-paramètres ont donc changé sur les versions qui ont suivi. La version 2 de \emph{YOLO} est encore aujourd'hui très utilisée et beaucoup mit en avant par la communauté.

    \subsubsection{L'implémentation}

    Plusieurs \emph{frameworks} permettant de faire des réseaux de neurones existent aujourd'hui sur le marché. L'un des plus connus est \emph{Tensorflow}\footnote{\url{https://www.tensorflow.org/}} développé par \emph{Google}. Les concepteurs de \emph{YOLO} n'ont pas décidé d'utiliser ce type de \emph{frameworks} mais de développer leur propre \emph{framework} appelé \emph{Darknet}\footnote{\url{https://pjreddie.com/darknet/}}.

    Avec la sortie des différents papiers sur le sujet, les concepteurs de cette technique de détection d'objets ont sorti les valeurs des poids de leur réseau de neurones entraîné sur certains jeux de données. Il est donc possible d'utiliser directement leur travail lorsque l'on souhaite détecter les mêmes classes sans faire de phases d'entraînement.

    \subsubsection{Entraînement du réseau avec ses propres données}

    Lorsque l'on souhaite entraîner le réseau avec ses propres données, il est possible de repartir des poids disponibles en ligne à condition qu'il s'agisse des mêmes classes ou de classes similaires (dans ce cas nous ferons de l'apprentissage par transfère ou \emph{Transfer Learning}\cite{wiki:Transfer_learning}).

    Dans notre cas, nous souhaitons entraîner le réseau à reconnaître des données assez différentes et surtout très similaires les unes par rapport aux autres. Il faudrait donc constituer un jeu de données labellisées très important avec tous les panneaux que nous souhaitons reconnaître. Il serait également nécessaire d'adapter le réseau au nombre de classes que nous souhaitons reconnaître.

    Une fois ce jeu de données constitué et l'architecture du réseau adapté à notre problématique, il serait enfin nécessaire de procéder aux coûteuses phases d'entraînement du réseau.

    J'ai tenté de commencer cette phase d'entraînement mais celle-ci nécessitait beaucoup trop de puissances de calcul pour que je puisse en arriver à bout avec un résultat correct dans le temps qui m'était imparti. En effet, une carte graphique de dernière génération est un plus pour entraîner ce type de réseau dont le nombre de paramètres est très conséquents. Dans la version 2 de YOLO pour la reconnaissance d'objets entraînée sur le \emph{Pascal VOC} il faut compter pas moins de 6 millions de paramètres entraînable qu'il faut modifier pour chaque image du jeu de données sur une centaine de phases d'entraînements. De plus, le temps de labelliser des images une par une est très conséquents et il faudrait plusieurs milliers d'images pour obtenir des résultats satisfaisants.

    \subsection{Limitations techniques}

    \subsubsection{Temps de calcul}

    Comme indiqué précédemment les temps de calcul lors de l'entraînement sont assez conséquent. Dans le cas de l'application finale, celle-ci n'aurait pas besoin de procéder à cette phase d'entraînement mais uniquement à exécuter les différents calculs permettant la prédiction pour chaque image avec les poids pré-entraînés. 

    Là encore, le temps de calcul dépendra énormément du nombre de classes et de la taille des images que nous souhaitons analyser. Il est aujourd'hui possible de faire tourner des systèmes de détection d'images sur des téléphones portables récents avec une vitesse de détection de l'ordre de l'image par seconde.

    Dans ce cas d'utilisation, il n'est pas nécessaire de détecter tous les éléments et d'avoir des données vraiment temps réel. En effet, ces données sont destinées à être ajoutées sur une cartographie mais surtout confirmées par les données récoltées par d'autres utilisateurs disposants également du système. Il serait donc possible de prendre une photo, de l'analyser et de reprendre une nouvelle photo dès que la phase de détection est terminée. Cette méthode permettrait de s'adapter au rythme de chaque appareil la puissance de calcul étant très variable d'un appareil à l'autre. 

    Enfin, il serait nécessaire d'allouer une capacité de calcul limitée pour éviter de perturber les autres usages de l'appareil, notamment dans notre cas la navigation. Cela ralentirai encore une fois le temps de calcul mais combiné avec la technique précédente cela ne serait pas véritablement un problème d'autant plus si on compte sur un nombre important d'utilisateurs.

    Pousser l'appareil à faire de nombreux calculs a de plus un impact direct sur la batterie. Il faudrait donc impérativement que l'appareil soit en charge pour ce type d'applications.

    \subsubsection{Poids de l'application}

    Dans le cas de notre application le poids de celle-ci est un facteur très important. Or, les poids du réseaux de neurones préalablement entraîné doivent être stockés dans celle-ci. Il faut compter pour cela un ajout d'environ 230 Mo supplémentaires. Une version de \emph{YOLO} allégée existe et permet de gagner en poids et en rapidité mais elle obtient des résultats bien moins satisfaisants.

    Il est donc difficilement envisageable d'intégrer cela directement à une application de navigation ou d'aide à la navigation. En effet, malgré l'augmentation continue des espaces de stockages sur ce type d'appareils beaucoup d'utilisateurs se plaignent du poids des applications que ça soit par manque d'espace de stockage ou par temps nécessaire pour appliquer les mises à jour.

    \subsubsection{Qualité des images capturées}

    Dans le cadre de ce projet, j'ai testé de prendre des images à l'aide de mon téléphone portable sur différents types de route, notamment par temps clair et durant la nuit (je n'ai malheureusement pas eu de situations pluvieuses). Les résultats obtenus sont bien supérieurs à mes attentes. La captation s'effectue dans une qualité suffisante à la lecture des panneaux y compris durant la nuit. Néanmoins la vitesse rend plus difficile la photographie correcte des panneaux.

    \section{Conclusion}

    \subsection{Mise en place du système}

    À la suite de cette étude et dans l'état actuel des choses je pense que ce type de systèmes n'est pas prêt à s'installer sur les applications mobiles grand public, notamment dû à l'espace de stockage nécessaire pour les poids du réseau de neurones. De plus une étude plus poussée est nécessaire pour déterminer l'impact de ce type de procédé exécuté en même temps qu'un système de navigation.

    Néanmoins, l'idée initiale pourrait être greffée sur le système des voitures qui sont déjà équipées d'outil de détection et reconnaissance de panneaux. L'arrivée des voitures connectées peuvent favoriser ce type d'usage. Certains constructeurs et équipementiers promettent déjà de développer des protocoles de communications inter-véhicules au développement de la 5ème génération de réseau cellulaire. L'évolution de ces technologies permettra probablement de mettre à jour les cartographies sur le principe initial décris précédemment. \\
    De plus, il serait également possible de faire une application mobile indépendante que seul les contributeurs accepteraient de télécharger et dans ce cas la question du poids de l'application serait moins un problème.

    \subsection{Point de vue personnel}

    Travailler sur ce projet était un grand changement pour moi. En effet, contrairement au projet que j'ai l'habitude de mener, je n'ai pas pu développer de démonstrations techniques concrètes. La partie technique s'est limitée à quelques tests d'implémentations de \emph{YOLO} et d'entraînement de ce type de réseau de neurones. 

    J'ai donc dans un premier temps été beaucoup moins stimulé par ce projet qui n'a finalement pas de réalisation concrète (selon moi). Néanmoins, j'ai trouvé intéressant le fait d'approfondir des aspects théoriques au travers de ce rapport car il s'agit là d'un exercice plutôt inhabituel pour moi (cela se ressent peut-être à la lecture de celui-ci). 
    \clearpage
    \bibliographystyle{unsrt}
    \bibliography{biblio_report}
\end{document}